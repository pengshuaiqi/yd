#  :fa-leanpub: 阅读｜书源分享
###  :fa-slack: [点击加入QQ频道【一程书友会】](https://qun.qq.com/qqweb/qunpro/share?_wv=3&_wwv=128&appChannel=share&inviteCode=P1sx8&appChannel=share&businessType=9&from=246610&biz=ka)

##  :fa-bullhorn: 更新通告

- 更新时间：05月01日（星期日）

##  :fa-stack-overflow: 书源

- 书源合集`321个`
- 更新时间：05月01日（星期日）
- 网络导入链接
- `因不可抗力 - 暂无链接`
- [网络导入 :fa-question: ](https://gitee.com/i-c/yd/issues/I57ID3)
- [阅读Pro - 下载地址](https://y-c.lanzoul.com/b05hl9sf)

##  :fa-rss: 订阅源

- 订阅源合集
- 更新时间：05月01日（星期日）
- 网络导入链接
```
https://e-c.coding.net/p/yicheng/d/YD/git/raw/master/dy.json
```
- [订阅源 - 下载地址](https://y-c.lanzoul.com/b05hl9sf)
- [订阅源 - 其他ǫᵀ](https://gitee.com/i-c/yd/issues/I55K8P)

##  :fa-arrows: 导入方案

- 建议删除旧书源再导入最新书源

##  :fa-film: 教程

###  :fa-play-circle-o: [规则订阅教程](https://b23.tv/PQosCT0)

###  :fa-file-code-o: 书源导入教程

1. 复制网络导入链接
2. 打开阅读APP
3. 在“我的”一栏选择“书源管理”
4. 点击右上角“ :fa-ellipsis-v: ”
5. 选择“网络导入”
6. 粘贴链接点确定
7. 弹出书源确定对话框，如果显示未全选，请先全选再点确定。

###  :fa-rss-square: 订阅源导入教程

1. 下载上面发布的“订阅源”
2. 打开阅读APP
3. 在“订阅”一栏选择右上角“ :fa-cog: ”图标
4. 点击右上角“ :fa-ellipsis-v: ”
5. 选择“本地导入”
6. 找到并选择下载好的文件
7. 弹出订阅源确定对话框，如果显示未全选，请先全选再点确定。

##  :fa-share-alt: 关于

- [开源阅读 - 阅读APP](https://www.coolapk.com/apk/io.legado.app.release)
- [源仓库 - 书源网站](http://www.yckceo.com/)
- [网站挂了吗 - 链接检测](https://gualemang.com/)
- [常用软件 - 科学必备](https://y-c.lanzoul.com/b00po7atg) 密码：`cyrj`

##  :fa-lightbulb-o: 温馨提示

- 本站所有内容仅供书友交流学习，勿做商用，如有侵权联系删除。